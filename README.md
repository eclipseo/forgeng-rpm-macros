<!--
SPDX-License-Identifier: GPL-1.0-or-later
-->

# forgeng-srpm-macros

These macros simplify the packaging of forge-hosted projects.
They automatically compute the Source urls based on macros set in the specfile.
This code has been split out from redhat-rpm-config to ease maintenance.

## License

This repository is licensed under

    SPDX-License-Identifer: GPL-1.0-or-later
