-- SPDX-License-Identifier: GPL-1.0-or-later
-- Copyright (C) 2018-2024 redhat-rpm-config contributors

-- Lua code used by macros.forge and derivatives; RPM stage

local fedora = require "fedora.common"
local  forge = require "fedora.srpm.forgeng"

-- This is constrained by the design of %apply_patch
local function apply_patches(suffix, verbose)
  local quiet = ""
  if not verbose then
    quiet = "-q "
  end
  for _, patch in ipairs(fedora.readlines("forge_patchlist" .. suffix)) do
    local ps = fedora.patch_suffix(patch)
    if ps then
      for _, o in ipairs({"patch_mode", "patch_level"}) do
        fedora.safeset(o .. ps, "%{forge_" .. o .. suffix .. "}")
      end
      -- %apply_patch does not have the notion of a disposable local currentfoo-like
      -- control variable, you need to override the main one
      fedora.set("__scm", "%{patch_mode" .. ps .. "}")
      -- %apply_patch requires specifying the patch to apply in 3 different forms!
      -- As a filename, as a full path, and as a patch suffix
      print(rpm.expand(
        "%apply_patch " ..  quiet .. "%{patch_level" .. ps .. "} " ..
        "-m " .. patch .. " %{_sourcedir}/" .. patch .. " " .. ps .. "\n"))
    end
  end
end

-- %forge_prep core
local function prep(suffix, verbose)
  fedora.id('forge.prep("' .. suffix .. '")')
  local quiet = ""
  if not verbose then
    quiet = "-q "
  end
  print(rpm.expand(
"%setup " .. quiet .. "%{forge_setup_arguments" .. suffix .. "}\n"
))
  apply_patches(suffix, verbose)
end

return {
  floop     = forge.floop,
  prep      = prep,
}
