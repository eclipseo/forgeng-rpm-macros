-- SPDX-License-Identifier: GPL-1.0-or-later
-- Copyright (C) 2018-2024 redhat-rpm-config contributors

-- Lua code used by macros.forge-srpm and derivatives

local fedora = require "fedora.common"

-- Computes the suffix of a version string, removing vprefix if it matches
-- For example with vprefix 1.2.3: 1.2.3.rc2 → .rc2 but 1.2.30 → 1.2.30 not 0
local function version_suffix(vstring, vprefix)
  if (vstring:sub(1, #vprefix) == vprefix) and
     (not string.match(vstring:sub(#vprefix + 1), "^%.?%d")) then
    return vstring:sub(#vprefix + 1)
  else
    return vstring
  end
end

-- Check if an identified url is sane
local function check_forge_url(url, id, silent)
  local checkedurl  = nil
  local checkedid   = nil
  local urlpatterns = {
      ["gitlab"] = {
        pattern     = 'https://[^#?]+',
        description = 'https://(…[-.])gitlab[-.]…/owner/repo'},
      ["pagure"] = {
        pattern     = 'https://[^/]+/[^/#?]+',
        description = 'https://pagure.io/repo'},
      ["pagure_ns"] = {
        pattern     = 'https://[^/]+/[^/]+/[^/#?]+',
        description = 'https://pagure.io/namespace/repo'},
      ["pagure_fork"] = {
        pattern     = 'https://[^/]+/fork/[^/]+/[^/#?]+',
        description = 'https://pagure.io/fork/owner/repo'},
      ["pagure_ns_fork"] = {
        pattern     = 'https://[^/]+/fork/[^/]+/[^/]+/[^/#?]+',
        description = 'https://pagure.io/fork/owner/namespace/repo'},
      ["gitea.com"] = {
        pattern     = 'https://[^/]+/[^/]+/[^/#?]+',
        description = 'https://gitea.com/owner/repo'},
      ["github"] = {
        pattern     = 'https://[^/]+/[^/]+/[^/#?]+',
        description = 'https://(…[-.])github[-.]…/owner/repo'},
      ["code.googlesource.com"] = {
        pattern     = 'https://code.googlesource.com/[^#?]*[^/#?]+',
        description = 'https://code.googlesource.com/…/repo'},
      ["bitbucket.org"] = {
        pattern     = 'https://[^/]+/[^/]+/[^/#?]+',
        description = 'https://bitbucket.org/owner/repo'},
      ["sourcehut"] = {
        pattern     = 'https://[^/]+/~[^/]+/[^/#?]+',
        description = 'https://git.sr.ht/~owner/repo'}}
  if (urlpatterns[id] ~= nil) then
    checkedurl = string.match(url, urlpatterns[id]["pattern"])
    if (checkedurl == nil) then
      if not silent then
        fedora.error(id .. " URLs must match " ..
                     urlpatterns[id]["description"] .. "!")
      end
    else
      checkedid = id
    end
  end
  return checkedurl, checkedid
end

-- Check if an url matches a known forge
local function idforge(url, silent, forge_override)
  local forge_url = nil
  local forge    = nil
  if (url ~= "") then
    forge = forge_override or string.match(url, "^[^:]+://([^/]+)/")
    if not forge then
      if not silent then
        fedora.error([[
URLs must include a protocol such as https:// and a path starting with /. Read:
]] .. url)
      end
    else
      if (forge == "pagure.io") then
        if     string.match(url, "[^:]+://pagure.io/fork/[^/]+/[^/]+/[^/]+") then
          forge = "pagure_ns_fork"
        elseif string.match(url, "[^:]+://pagure.io/fork/[^/]+/[^/]+") then
          forge = "pagure_fork"
        elseif  string.match(url, "[^:]+://pagure.io/[^/]+/[^/]+") then
          forge = "pagure_ns"
        elseif  string.match(url, "[^:]+://pagure.io/[^/]+") then
          forge = "pagure"
        end
      elseif (string.match(forge, "^gitlab[%.-]") or string.match(forge, "[%.-]gitlab[%.]")) then
        forge = "gitlab"
      elseif (string.match(forge, "^github[%.-]") or string.match(forge, "[%.-]github[%.]")) then
        forge = "github"
      elseif  string.match(url, "[^:]+://git.sr.ht/") then
        forge = "sourcehut"
      elseif (forge == "codeberg.org") then
        forge = "gitea.com"
      end
      forge_url, forge = check_forge_url(url, forge, silent)
    end
  end
  return forge_url, forge
end

-- rpm variable radicals
local rads = {
  -- Elements that will be pushed SRPM-level
  ["source"] = {"version", "url"},
  ["forge"]  = {
    -- key variables that may control a declaration block
    ["key"]      = {"url"},
    -- variables, used to compute a SCM reference
    ["ref"]      = {"tag", "commit", "branch", "version", "ref"},
    -- other computed or recomputed variables
    ["computed"] = {"tag", "source", "source_suffix", "setup_arguments",
                    "patch_mode", "patch_level", "extract_dir",
                    "time", "date", "file_ref"},},
}

-- Return all the suffixes for which one of the forge key variables is set
local function suffixes()
  return fedora.all_suffixes(fedora.qualify(rads["forge"]["key"], "forge"))
end

-- Executes the f function in a loop
--   – the loop is controled by index (a string):
--     — if the index value given to floop is nil, loop over the whole
--       range. Use readflag() to process rpm flag arguments safely.
--     — otherwise execute f for the specified index value only.
--   – f is passed index then otherargs as arguments.
--   – the index range is controlled by suffixes()
local function floop(f, index, otherargs)
  fedora.floop(f, index, suffixes(), otherargs)
end

-- forge variable derivation patterns
local function rules(suffix)
  local R = {
    ["default"] = {
      ["scm"]           = "git",
      ["archive_extension"]  = "tar.bz2",
      ["repo"]          = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("^[^:]+://[^/]+/[^/]+/([^/?#]+)"))}',
      ["archive_name"]  = "%{forge_repo"         .. suffix .. "}-%{forge_ref" .. suffix .. "}",
      ["top_dir"]       = "%{forge_archive_name" .. suffix .. "}" },
    ["gitlab"] = {
      ["source"]  = "%{forge_url" .. suffix .. "}/-/archive/%{forge_ref" .. suffix .. "}/%{forge_archive_name" .. suffix .. "}.%{forge_archive_extension" .. suffix .. "}" },
    ["pagure"] = {
      ["archive_extension"]  = "tar.gz",
      ["repo"]          = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("^[^:]+://[^/]+/([^/?#]+)"))}',
      ["source"]        = "%{forge_url" .. suffix ..   "}/archive/%{forge_ref" .. suffix .. "}/%{forge_archive_name" .. suffix .. "}.%{forge_archive_extension" .. suffix .. "}" },
    ["pagure_ns"] = {
      ["archive_extension"]  = "tar.gz",
      ["namespace"]    = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("^[^:]+://[^/]+/([^/]+)/[^/?#]+"))}',
      ["repo"]         = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("^[^:]+://[^/]+/[^/]+/([^/?#]+)"))}',
      ["archive_name"] = "%{forge_namespace" .. suffix .. "}-%{forge_repo" .. suffix .. "}-%{forge_ref" .. suffix .. "}",
      ["source"]       = "%{forge_url" .. suffix .. "}/archive/%{forge_ref"   .. suffix .. "}/%{forge_archive_name" .. suffix .. "}.%{forge_archive_extension" .. suffix .. "}" },
    ["pagure_fork"] = {
      ["archive_extension"]  = "tar.gz",
      ["owner"]        = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("https://[^/]+/fork/([^/]+)/[^/?#]+"))}',
      ["repo"]         = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("https://[^/]+/fork/[^/]+/([^/?#]+)"))}',
      ["archive_name"] = "%{forge_owner" .. suffix .. "}-%{forge_repo" .. suffix .. "}-%{forge_ref" .. suffix .. "}",
      ["source"]  = "%{forge_url" .. suffix .. "}/archive/%{forge_ref" .. suffix .. "}/%{forge_archive_name" .. suffix .. "}.%{forge_archive_extension" .. suffix .. "}" },
    ["pagure_ns_fork"] = {
      ["archive_extension"]  = "tar.gz",
      ["owner"]        = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("https://[^/]+/fork/([^/]+)/[^/]+/[^/?#]+"))}',
      ["namespace"]    = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("https://[^/]+/fork/[^/]+/([^/]+)/[^/?#]+")}',
      ["repo"]         = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("https://[^/]+/fork/[^/]+/[^/]+/([^/?#]+)")}',
      ["archive_name"] = "%{forge_owner" .. suffix .. "}-%{forge_namespace" .. suffix .. "}-%{forge_repo" .. suffix .. "}-%{forge_ref" .. suffix .. "}",
      ["source"]       = "%{forge_url" .. suffix .. "}/archive/%{forge_ref" .. suffix .. "}/%{forge_archive_name" .. suffix .. "}.%{forge_archive_extension" .. suffix .. "}" },
    ["gitea.com"]  = {
      ["archive_extension"]  = "tar.gz",
      ["archive_name"]       = "%{forge_file_ref" .. suffix .. "}",
      ["source"]             = "%{forge_url" .. suffix .. "}/archive/%{forge_ref" .. suffix .. "}.%{forge_archive_extension" .. suffix .. "}",
      ["top_dir"]            = "%{forge_repo}" },
    ["github"] = {
      ["archive_extension"]  = "tar.gz",
      ["archive_name"]       = "%{forge_repo" .. suffix .. "}-%{forge_file_ref" .. suffix .. "}",
      ["source"]             = "%{forge_url"  .. suffix .. "}/archive/%{forge_ref" .. suffix .. "}/%{forge_archive_name" .. suffix .. "}.%{forge_archive_extension" .. suffix .. "}" },
    ["code.googlesource.com"] = {
      ["archive_extension"]  = "tar.gz",
      ["repo"]               = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("^[^:]+://.+/([^/?#]+)"))}',
      ["source"]             = "%{forge_url" .. suffix .. "}/+archive/%{forge_ref" .. suffix .. "}.%{forge_archive_extension"  .. suffix .. "}",
      ["top_dir"]            = "" },
    ["bitbucket.org"] = {
      ["short_commit"] = '%{lua:print(string.sub(rpm.expand("%{forge_commit' .. suffix .. '}"), 1, 12))}',
      ["owner"]        = '%{lua:print(rpm.expand("%{forge_url' .. suffix .. '}"):match("^[^:]+://[^/]+/([^/?#]+)"))}',
      ["archive_name"] = "%{forge_owner" .. suffix .. "}-%{forge_repo" .. suffix .. "}-%{forge_short_commit" .. suffix .. "}",
      ["source"]       = "%{forge_url" .. suffix .. "}/get/%{forge_ref" .. suffix .. "}.%{forge_archive_extension"  .. suffix .. "}" } }
  return R
end

-- Returns forge radicals
local function radicals(suffix)
  local R = {}
  for k, v in pairs(rads["forge"]) do
    R[k] = v
  end
  R["rules"] = {}
  for _, v in pairs(rules(suffix)) do
    local l = {}
    for k, _ in pairs(v) do
      table.insert(l, k)
    end
    R["rules"] = fedora.mergelists({R["rules"], l})
  end
  R["computed"] = fedora.mergelists({R["computed"], R["key"], R["rules"]})
  local all = {}
  for k, v in pairs(R) do
    all = fedora.mergelists({all, v})
  end
  R["all"] = all
  return R
end

-- %forge_init core
local function init(suffix, verbose, informative, silent)
  fedora.id('forge.init("' .. suffix .. '")')
  local ismain = (suffix == "") or (suffix == "0")
  local myrads = radicals(suffix)
  print([[
# forge_init ]] .. suffix .. [[
]])
  local spec = {}
  for _, v in ipairs(fedora.qualify(myrads["all"], "forge")) do
    spec[v] = fedora.read(v .. suffix)
  end
  -- Compute the reference of the object to fetch
  if not (spec["forge_tag"] or spec["forge_commit"] or spec["forge_branch"] or
          spec["forge_version"]) then
    fedora.error([[
You need to set one of:
  – %%{forge_tag]]     .. suffix .. [[}
  – %%{forge_commit]]  .. suffix .. [[}
with your:
%%{forge_url]] .. suffix .. [[} %{?forge_url]] .. suffix .. [[}
declaration.
]])
  end
  local keep_version =  "true"
  local keep_file_ref = "true"
  local forge_url = spec["forge_url"]
  local forge
  forge_url, forge = idforge(forge_url, silent)
  if forge then
    if (forge == "github") or (forge == "code.googlesource.com") then
      keep_version = "false"
    elseif (forge == "bitbucket.org") and not spec["forge_commit"] then
      fedora.error("All BitBucket URLs require commit value knowledge: " ..
                   "you need to set %{forge_commit}!")
    end
    if (forge == "github") then
      keep_file_ref = "false"
    end
  end
  local ref = [[%{lua:
    local       fedora = require "fedora.common"
    local       suffix = "]] .. suffix       .. [["
    local keep_version = "]] .. keep_version .. [["
    local r = "tag"
    for _, k in ipairs({"tag", "commit", "branch", "version"}) do
      if fedora.read("forge_" .. k .. suffix) then
        r = k
        break
      end
    end
    if (r == "version") and
       not (keep_version == "true") then
      r = "v%{forge_" .. r .. suffix .. "}"
    else
      r =  "%{forge_" .. r .. suffix .. "}"
    end
    print(r)
    }]]
  local file_ref = [[%{lua:
    local        fedora = require "fedora.common"
    local           ref = [=[]] .. ref           .. [[]=]
    local        suffix =   "]] .. suffix        .. [["
    local keep_file_ref =   "]] .. keep_file_ref .. [["
    local f = fedora.expand(ref)
    local c = fedora.read("forge_commit" .. suffix)
    if (not (c and (c == f))) and
        f:match("^v[%d]") and
        not (keep_file_ref == "true") then
      f = f:gsub("^v", "")
    end
    f = f:gsub("/", "-")
    print(f)
    }]]
  fedora.mset(myrads["all"], "forge", suffix,
    { { ["url"]      = forge_url,
        ["ref"]      = ref,
        ["file_ref"] = file_ref },
      rules(suffix)[forge], rules(suffix)["default"] }, verbose)
  -- Update lua copy with the result
  for _, v in ipairs(fedora.qualify(myrads["rules"], "forge")) do
    spec[v] = fedora.read(v .. suffix)
  end
  -- Source URL processing
  local source = "%{forge_source" .. suffix .. "}"
  local archive_file = "%{forge_archive_name" .. suffix ..
                       "}.%{forge_archive_extension" .. suffix .. "}"
  if (fedora.expand(source):match("/([^/]+)$")
      ~= fedora.expand(archive_file)) then
    source = source .. "#/" .. archive_file
  end
  -- Setup processing
  local setup_arguments = "-n %{forge_extract_dir" .. suffix .. "}"
  local extract_dir     = "%{forge_top_dir"        .. suffix .. "}"
  if (spec["forge_top_dir"] == "") then
    setup_arguments     = "-c " .. setup_arguments
    extract_dir         = "%{archivename"   .. suffix .. "}"
  end
  if not ismain then
    if (spec["forge_top_dir"] ~= "") then
      setup_arguments = "-T -D -b " .. suffix .. " " .. setup_arguments
    else
      setup_arguments = "-T -D -a " .. suffix .. " " .. setup_arguments
    end
  end
  local time
  if spec["forge_date"] then
    time = [[%{lua:
      local fedora = require "fedora.common"
      local suffix = "]] .. suffix .. [["
      local   date = fedora.read("forge_date" .. suffix)
      if date and (date ~= "") then
        rpm.expand("%(date -u --iso-8601=seconds -d " .. date .. ")")
      end
    }]]
  else
    time = "%([ -r %{_sourcedir}/" .. archive_file .. " ] && " ..
           "date -u --iso-8601=seconds -r %{_sourcedir}/" ..
           archive_file .. ")"
  end
  local date = '%{?forge_time' .. suffix .. ':' ..
               '%([[ -n "%{forge_time' .. suffix .. '}" ]] && ' ..
               'date -u +%Y%m%d -d %{forge_time' .. suffix .. '})}'
  fedora.mset(myrads["all"], "forge", suffix, {{
      ["source"]            = source,
      ["source_suffix"]     = source_suffix,
      ["extract_dir"]       = extract_dir,
      ["setup_arguments"]   = setup_arguments,
      -- Due to the design of %apply_patch we will need to set %{__scm} from
      -- %{forge_patch_modeX} later. Therefore, expand the fallback to avoid
      -- loops and inter-block side effects
      ["patch_mode"]        = fedora.read("__scm"),
      ["patch_level"]       = "-p 1",
      ["date"]              = date,
      ["time"]              = time,
    }}, verbose)
  -- set srpm values if not set yet
  fedora.alias(rads["source"], "forge", suffix, "source", "", verbose)
  -- dist processing
  local distprefix = [[%{lua:
    local fedora = require "fedora.common"
    local    ref = [=[]] .. ref    .. [[]=]
    local suffix =   "]] .. suffix .. [["
    local d = fedora.expand(ref)
    local c = fedora.read("forge_commit" .. suffix)
    if c and (c == d) then
      d = d:sub(1, 7)
    else
      local forge = require "fedora.srpm.forge"
      local     v = fedora.read("source_version")
      if v then
        if not v:match("%%") then
          v = v:lower():gsub("[%p%s]+", ".")
        end
        if not d:match("%%") then
          d = d:lower():gsub("[%p%s]+", ".")
        end
      end
    end
    if (d ~= "") then
      d = ".%{?forge_scm" .. suffix .. "}" ..
          "%{?forge_date" .. suffix .. "}" .. d
      d = fedora.expand(d)
      if not d:match("%%") then
        d = d:lower():gsub("[%p%s]+", ".")
      end
      print(d)
    end
  }]]
  distprefix_suffix = fedora.suffix({
    ["distprefix"] = distprefix}, true, verbose)
  -- Final spec variable summary if the macro was called with -i
  if informative then
    fedora.echo("Variables read or set by %%forge_init")
    fedora.echovars(fedora.qualify(myrads["all"], "forge"), suffix)
    if distprefix_suffix then
      fedora.echovars({"distprefix"}, distprefix_suffix)
    end
    fedora.echo([[
Note: unless explicitly set, snapshot timestamp will be computed once
%%{_sourcedir}/]] .. archive_file .. [[

is available.
]])
  end
end

-- post-init info that does not depend on a particular forge suffix
local function info_generic(informative)
  if informative then
    fedora.echo("Other variables, that may have been set by %%forge_init")
    fedora.echovars(fedora.qualify(rads["source"], "source"), "")
  end
end

-- %forge_sources core
local function source(suffix)
  fedora.id('forge.source("' .. suffix .. '")')
  print(rpm.expand([[
%{?forge_source]] .. suffix .. [[}
]]))
end

-- %forge_patches core
local function patchlist(suffix)
  fedora.id('forge.patchlist("' .. suffix .. '")')
  print(rpm.expand([[
%{?forge_patchlist]] .. suffix .. "}"))
end

local function env(suffix, verbose)
  local myrads = radicals(suffix)
  fedora.set_current(fedora.qualify(myrads["all"], "forgeng"), suffix,
                     verbose)
  fedora.set_verbose(verbose)
end

return {
  version_suffix = version_suffix,
  floop          = floop,
  init           = init,
  info_generic   = info_generic,
  env            = env,
  source         = source,
  patchlist      = patchlist
}
